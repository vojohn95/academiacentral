<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'quiz_liveviewgrid', language 'es_mx', branch 'MOODLE_37_STABLE'
 *
 * @package   quiz_liveviewgrid
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allgroups'] = 'Todos los grupos';
$string['allquestions'] = 'O regresar a Todas las preguntas';
$string['choosegroup'] = 'Elegir un grupo';
$string['choosequestion'] = 'Elegir una pregunta (navegar)';
$string['clickcompact'] = 'Clic &quot;Hacer tabla compacta&quot; para obtener una vista compacta de respuestas del estudiante.';
$string['clickhideanswer'] = 'Clic para ocultar respuesta correcta';
$string['clickhidekey'] = 'Clic para ocultar el color de clave/leyenda para las calificaciones.';
$string['clickhidenames'] = 'Clic para ocultar nombres de estudiantes.';
$string['clickorderfirstname'] = 'Clic para ordenar los nombres en la tabla por Nombre.';
$string['clickorderlastname'] = 'Clic para ordenar los nombres en la tabla por Apellido.';
$string['clickshowanswer'] = 'Clic para mostrar respuesta correcta';
$string['clickshowkey'] = 'Clic para mostrar el color de clave/leyenda para las calificaciones.';
$string['clickshownames'] = 'Clic para mostrar nombres de estudiantes.';
$string['clicksingleq'] = 'Clic aquí para ir a la vista de pregunta única para pregunta';
$string['compact'] = 'Hacer tabla compacta';
$string['dynamicpage'] = '-- Página del Reporte en Vivo Dinámico';
$string['expandexplain'] = 'Clic &quot;Expander Tabla&quot; para regresar de la vista compacta.';
$string['expandtable'] = 'Expander tabla';
$string['firstname'] = 'Nombre';
$string['fractioncolors'] = 'Colores de Fracción';
$string['from'] = 'de';
$string['gradedexplain'] = 'Si un estudiante no ha terminado el examen o si hace varios intentos, la  "Calificación" no es la calificación para el examen.';
$string['hidecorrectanswer'] = 'Ocultar respuesta correcta';
$string['hidegradekey'] = 'Ocultar Leyenda de Calificación';
$string['hidegrades'] = 'Ocultar la  &quot;Calificación&quot; respuestas que recibiría';
$string['hidenames'] = 'Ocultar nombres de estudiantes.';
$string['lastname'] = 'Apellido(s)';
$string['liveviewgrid'] = 'Reporte Vivo';
$string['liveviewgrid:componentname'] = 'Reporte en Vivo';
$string['liveviewgrid:view'] = 'Ver Reporte en Vivo';
$string['newpage'] = 'Nueva página, estática e imprimible';
$string['notallowedgroup'] = '\\n<br />Usted no puede ver estos resultados.
O porque Usted no puede ver todos los grupos o porque Usted está intentando ver respuestas de estudiantes en un grupo al cual no tiene permitido ver.';
$string['notice'] = 'Aviso';
$string['noticeexplain'] = 'La mayoría de las páginas del Reporte en Vivo son dinámica. Esta página NO LO ES.
Usted debe hacer clic en un botón de opción o refrescar la página para mostrar las respuestas del estudiante enviadas después de que la página cargó.
Esta página es útil si Usted quiere comparar respuestas en diferentes momentos o si quiere imprimir resultados.
Use la Página del Reporte en Vivo Dinámico para navegar hacia otras páginas. Las respuestas de';
$string['notmember'] = 'Usted no es un miembro de este grupo.\\n<br />';
$string['numberofresponses'] = 'Número de Respuestas';
$string['or'] = 'o';
$string['orderfirstname'] = 'Ordenar tabla por nombre';
$string['orderlastname'] = 'Ordenar tabla por Apellido';
$string['pickgroup'] = 'Usted debe elegir un grupo';
$string['pluginname'] = 'Live Report (Reporte Vivo)';
$string['popoutinfo'] = 'Clic para dar una página nueva que es estática. Usted puede usarla para comparar respuestas a ddiferentes tiempos y para imprimir resultados';
$string['printinfo'] = 'Clic para imprimir esta página o guardar esta página como un documento PDF.';
$string['printpage'] = 'Imprimir Página';
$string['privacy:metadata'] = 'El plugin del Reporte Vivo del Examen \'Quiz Live Report0 (liveviewgrid) no almacena ningún dato personal. Proporciona una interfase para que los profesores vean datos de los exámenes sin almacenar ningún dato por sí mismo.';
$string['questionis'] = 'La pregunta es:';
$string['questionresponses'] = 'Respuestas para Reporte en Vivo';
$string['quizname'] = 'Nombre del examen';
$string['refreshpage'] = '¡Refrescar Página!';
$string['responses'] = 'Respuestas';
$string['rightanswer'] = 'Respuesta Correcta:';
$string['showcorrectanswer'] = 'Mostrar respuesta correcta';
$string['showgradekey'] = 'Leyenda Mostrar Calificación';
$string['showgrades'] = 'Mostrar la "Calificación" respuestas que recibiría';
$string['showgradetitle'] = 'Esto muestra si una respuesta es correcta pero no califica al examen';
$string['shownames'] = 'Mostrar nombres de estudiantes.';
$string['somethingiswrongwithanswerid'] = 'Algo está mal con la respuesta con id =';
$string['staticpage'] = '-- Página Estática';
$string['whichgroups'] = '¿Cual grupo quiere ver?';
$string['whichquestion'] = '¿Cual pregunta quiere ver?';
$string['youmustbeauthorized'] = 'Usted necesita estar autorizado para acceder a este sitio.';
$string['youmustsubmitquestonid'] = 'Usted debe enviar una questionid válida.';
